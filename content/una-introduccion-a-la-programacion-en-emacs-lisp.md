Title: Una Introducción a la programación en Emacs LISP
Date: 2016-12-24 00:00
Slug: una-introduccion-a-la-programacion-en-emacs-lisp

Este es el primer libro que editamos. La traducción del libro ha sido
hecha por Libremanuals. Es un libro muy paradigmático de la comunidad
(emacs-es) donde los hispanohablantes se acercan a realizar impulsos de
traducción o lectura del libro para ser mejores usuarios y programadores
de emacs.

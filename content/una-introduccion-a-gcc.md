Title: Una Introducción a GCC
Date: 2016-10-06 00:00
Slug: una-introduccion-a-gcc

Este manual ofrece una introducción tutorial completa a los compiladores
GNU C y C++, gcc y g++. Los compiladores permiten pasar de código fuente
a código objeto (por decirlo con una metáfora de recetas de cocina a
comida real).

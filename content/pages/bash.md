Date: 2018-06-16
Slug: bash.html
Title: Manual de Referencia de Bash
Img: img/bash.png
ISBN: 978-84-09-02741-5
Pages: 203
Price: 15 €
Status: draft

Este manual ofrece una descripción completa al lenguaje Bash.

Bash es el mejor lenguaje para iniciarse en la programación desde una
pequeña motivación práctica en el mundo UNIX. Una vez que una persona
aprende los comandos básicos suele resultar práctico aprender bucles o
condicionales y almacenar ese trabajo en un fichero. Para ese pequeño
trabajo Bash es el lenguaje más indicado. Trabajos básicos clásicos en
Unix como generar copias de seguridad suelen realizarse en este
lenguaje.

Bash es un lenguaje de programación estructurado para administradores/as
de sistemas escrito inicialmente por Brian Fox. Fue generado desde el
proyecto GNU para su shell. Si bien hay muchas shell para Unix, esta es
la que viene preinstalada en las distribuciones GNU/Linux más populares
como Debian o Ubuntu.

Desde Libremanuals hemos hecho el esfuerzo de traducción de este manual
y distribuimos el resultado desde un [repositorio git
público](https://github.com/davidam/bashrefes/). Admitimos las mejoras
constructivas que la comunidad considere adecuadas.

Hemos escrito algunos [ejemplos de
Bash](https://github.com/davidam/bash-examples) para demostrar algunos
usos del lenguaje que incentiven la lectura del libro.

-   ISBN: 978-84-09-02741-5
-   Número de Páginas: 203
-   <span property="price">15</span>€

Author: David Arroyo Menéndez
Date: 2018-06-19
ISBN: 978-84-608-8894-9
Img: img/orgmode-front.png
Pages: 50
Price: 8 €
Save_as: org-mode.html
Short_title: Org-Mode
Title: Guía Compacta de Org-Mode
Url: org-mode.html

[Org mode](http://orgmode.org/) es un sistema poderoso para organizar proyectos, tareas y
notas en el editor [GNU/Emacs](https://www.gnu.org/software/emacs/), principalmente. Soporta ítems de
contenidos (*outlines*), planificación de proyectos, y documentos de
autoría con un rápido y efectivo sistema basado en texto plano.

Org mode almacena todo en ficheros de texto plano legibles para
humanos, asegurando la portabilidad completa, integración simple con
otras herramientas de procesamiento de texto y soporte para trazar la
revisión y sincronización usando cualquier sistema de control de
versiones.

Org mode es a GNU/Emacs, lo que GNU/Emacs es al sistema
operativo. Esto es, un sustrato en el que apoyarse para todo tipo de
operaciones: documentos, presentaciones, hiperenlaces, agenda,
etc. Hay cuestiones avanzadas como tener código fuente en el documento
y en el documento objeto, por ejemplo, un pdf, se muestre el resultado
que no se verán (por ahora) en otros sistemas de documentación como
Word, Latex, etc.

- ISBN: 978-84-608-8894-9
- Número de páginas: 50
- Precio: 8€

Ya puedes comprarlo en

- [UAM](https://www.libros.so/libro/guia-compacta-de-org-mode_1090952)
- [URJC](https://www.libros.so/libro/guia-compacta-de-org-mode_1090952)
- [Trafıcantes de Sueños](https://www.traficantes.net/libros/guia-compacta-de-org-mode)
- [FAL](http://fal.cnt.es/tienda/node/704)
- [La Rosa Negra](http://larosanegraediciones.com/ciencia-y-tecnologia/881-guia-compacta-de-org-mode.html?search_query=Guia+compacta+de+org-mode&results=1)

Puedes discutir dudas o detalles del libro desde la [lista de distribución orgmode creada para Libremanuals](https://lists.riseup.net/www/info/orgmode). Y descargar las fuentes del libro desde [GitHub](https://github.com/davidam/orgguide-es). También hemos hecho algunas contribuciones a org-mode que sirven de ejemplo para iniciarse en la programación de este programa para GNU Emacs:

- [Org License](https://code.orgmode.org/bzg/org-mode/src/master/contrib/lisp/org-license.el)
- [Org Effectiveness](https://code.orgmode.org/bzg/org-mode/src/master/contrib/lisp/org-effectiveness.el)

Author: David Arroyo Menéndez
Date: 2018-01-05
ISBN: 978-84-608-7274-0
Img: img/gcc.png
Pages: 157
Price: 15 €
Url: gcc.html
Save_as: gcc.html
Short_title: GCC
Title: Una Introducción a GCC

Este manual ofrece una introducción tutorial completa a los compiladores
GNU C y C++, gcc y g++. Los compiladores permiten pasar de código fuente
a código objeto (por decirlo con una metáfora de recetas de cocina a
comida real).

Muchos libros enseñan lenguajes C y C++, este libro enseña cómo usar el
compilador en sí. Todos los problemas comunes y mensajes de error
encontrados por nuevos usuarios de GCC son cuidadosamente explicados con
numerosos ejemplos "Hola mundo" fáciles de seguir.

Se pueden solicitar mejoras al libro a través de
[Savannah](http://savannah.nongnu.org/projects/gccintro-es). Y compras
en [UAM](https://www.libros.so/libro/una-introduccion-a-gcc_944478) y
[Traficantes de Sueños](https://www.traficantes.net/libros/una-introduccion-gcc).

- ISBN: 978-84-608-7274-0.
- Número de Páginas: 157.
- Precio: 15€

Ya puedes comprarlo en

- [UAM](https://www.libros.so/libro/una-introduccion-a-gcc_944478)
- [URJC](https://www.libros.so/libro/una-introduccion-a-gcc_944478)
- [Trafıcantes de Sueños](https://www.traficantes.net/libros/una-introduccion-gcc)
- [FAL](http://fal.cnt.es/tienda/node/703)
- [La Rosa Negra](http://larosanegraediciones.com/ciencia-y-tecnologia/884-una-introduccion-a-gcc.html)

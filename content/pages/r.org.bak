#+TITLE: Una introducción a R
#+DATE: 2018-01-18
#+AUTHOR: David Arroyo Menéndez
#+PROPERTY SLUG r.html
#+PROPERTY IMG /img/R-front.png
#+PROPERTY IMG_DESC Una introducción a R
#+PROPERTY ISBN 978-84-608-5935-2
#+PROPERTY PAGES 265
#+PROPERTY PRICE 15 €

Este manual ofrece una introducción tutorial completa al lenguaje GNU R.

Este manual ofrece una introducción tutorial completa al lenguaje GNU R. GNU R
es el lenguaje estadístico más usado (Fuente:
[[https://www.tiobe.com/tiobe-index/][Tiobe, 2017]]). Ningún otro lenguaje
estadístico tiene tantos módulos.

R está escrito en C, Fortran y R y liberado con la licencia GPL. R
tiene una línea de comandos y varios front-ends gráficos disponibles.

R es multiparadigma: arrays, orientado a objetos, imperativo,
funcional, procedural, con capacidad de reflexión.

La comunidad de R es fuerte, se hacen conferencias cada año y tiene su propia
revista científica: [[https://journal.r-project.org/][R Journal]]

La comunidad de R está concienciada con la promoción de la diversidad de género
a través de [[https://rladies.org][rladies]].

El libro es desarrollado por su comunidad (R Development Team) y
traducido por la comunidad hispanohablante de R (Andrés González y
Silvia González).

- ISBN: 978-84-697-7122-8</li>
- Número de Páginas: 100</li>
- 15 €

Ya puedes comprarlo en
- [[http://larosanegraediciones.com/ciencia-y-tecnologia/896-una-introduccion-a-r.html][La Rosa Negra]]
- [[https://www.traficantes.net/libros/una-introduccion-r][Traficantes de Sueños]]
- [[http://fal.cnt.es/tienda/node/702][FAL]]
- [[https://www.libros.so/libro/una-introduccion-a-r_1081725][URJC]]
- [[https://www.libros.so/libro/una-introduccion-a-r_1081725][UAM]]

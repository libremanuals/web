#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Libremanuals'
SITENAME = 'Libremanuals'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH='public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

THEME = 'theme/'

# URL settings (http://docs.getpelican.com/en/stable/settings.html#url-settings)
RELATIVE_URLS = True
STATIC_PATHS = ['merchandising', 'img', 'slides']

# PLUGIN_PATHS=['plugins']
# PLUGINS=['org_reader']
#
# ORGMODE = {
#     'code_highlight': False,
# }

Author: David Arroyo Menéndez
Date: 2018-01-18
ISBN: 978-84-608-5935-2
Img: img/R-front.png
Pages: 265
Price: 15 €
Save_as: r.html
Title: Una introducción a R
Url: r.html

Este manual ofrece una introducción tutorial completa al lenguaje GNU R.

Este manual ofrece una introducción tutorial completa al lenguaje GNU R. GNU R
es el lenguaje estadístico más usado (Fuente:
[Tiobe, 2017](https://www.tiobe.com/tiobe-index/). Ningún otro lenguaje
estadístico tiene tantos módulos.

R está escrito en C, Fortran y R y liberado con la licencia GPL. R
tiene una línea de comandos y varios front-ends gráficos disponibles.

R es multiparadigma: arrays, orientado a objetos, imperativo,
funcional, procedural, con capacidad de reflexión.

La comunidad de R es fuerte, se hacen conferencias cada año y tiene su propia
revista científica: [R Journal](https://journal.r-project.org/)

La comunidad de R está concienciada con la promoción de la diversidad de género
a través de [rladies](https://rladies.org).

El libro es desarrollado por su comunidad (R Development Team) y
traducido por la comunidad hispanohablante de R (Andrés González y
Silvia González).

- ISBN: 978-84-697-7122-8
- Número de Páginas: 100
- 15 €

Ya puedes comprarlo en

- [La Rosa Negra](http://larosanegraediciones.com/ciencia-y-tecnologia/896-una-introduccion-a-r.html)
- [Traficantes de Sueños](https://www.traficantes.net/libros/una-introduccion-r)
- [FAL](http://fal.cnt.es/tienda/node/702)
- [URJC](https://www.libros.so/libro/una-introduccion-a-r_1081725)
- [UAM](https://www.libros.so/libro/una-introduccion-a-r_1081725)

Author: David Arroyo Menéndez
Date: 2018-06-27
ISBN: 978-84-09-01157-5
Img: img/python-front.png
Pages: 102
Price: 15 €
Save_as: python.html
Short_title: Python
Title: El Tutorial de Python
Url: python.html

Este manual ofrece una introducción tutorial completa al lenguaje
Python.

Python es el cuarto lenguaje de programación más usado en el mundo
(Fuente: [Tiobe, 2018](https://www.tiobe.com/tiobe-index/)). Siendo el
lenguaje de script preferido entre la comunidad de programadores
internacional.

Python destaca como lenguaje de programación en áreas como la
administración de sistemas, inteligencia artificial y la ciencia en
general. También tiene una comunidad amplia en otras áreas como la
programación web, videojuegos, ...

Python tiene diferentes paradigmas: orientado a objetos, imperativo,
funcional y reflexivo

Python fué diseñado por Guido Van Rossum y se financia a través de la
Python Software Foundation. Posee una licencia de código abierto,
denominada Python Software Foundation License, que es compatible con la
licencia GPL.

La comunidad de Python está concienciada con la promoción de la
diversidad de género a través de [pyladies](https://pyladies.org).

El libro es desarrollado por Guido Van Rossum y traducido por la
comunidad argentina de Python (Octubre, 2017).

Hemos seleccionado un [buen número de ejemplos
python](https://github.com/davidam/python-examples), para demostrar las
capacidades del lenguaje en las diferentes áreas de la computación.

- ISBN: 978-84-09-01157-5
- Número de Páginas: 102
- <span property="price">15</span>€

Ya puedes comprarlo en

- [Fundación Anselmo Lorenzo](http://fal.cnt.es/tienda/node/720)
- [URJC](https://www.libros.so/libro/el-tutorial-de-python_1160684)
- [UAM](https://www.libros.so/libro/el-tutorial-de-python_1160684)
- [Rosa
    Negra](http://larosanegraediciones.com/ciencia-y-tecnologia/1039-el-tutorial-de-python.html)

Puede usted preguntar dudas acerca de Python en la [comunidad
hispana](https://lists.es.python.org/listinfo).

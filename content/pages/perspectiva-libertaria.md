Author: David Arroyo Menéndez
Title: Perspectiva Libertaria de Libremanuals
Save_as: perspectiva-libertaria.html
Status: hidden
Url: perspectiva-libertaria.html

El proyecto GNU surge al igual que el movimiento ecologista para
resolver una problemática concreta de la sociedad, en su caso la
libertad en el mundo del software, desde la cultura científica de
compartición de conocimientos matemáticos y revisión de iguales, se
opone a la cultura del software privativo alguna de las cuatro
libertades que GNU considera esenciales en el mundo del software:

-   Libertad de uso
-   Libertad de copia
-   Libertad de modificación
-   Libertad de redistribuir lo modificado

Libremanuals recoge esa filosofía con una perspectiva obrera, esto es,
la necesidad del software libre debe poderse explicar no a los
científicos de la computación, sino a trabajadores de cualquier ramo de
la industria.

La industria del software corresponde a una necesidad económica: al
hacer cálculos con máquinas se facilita la gestión de la informácion
(ej: Internet), la rapidez en grandes cálculos (ej: supercomputación),
pero también en cálculos domésticos (ej: hojas de cálculos). Delegar
estos cálculos a grandes corporaciones de maneras no verificables por
cualquier compañero de, por ejemplo, un sindicato de tu ciudad que tenga
suficiente inteligencia. Nos lleva a tener la gestión de nuestras vidas
a merced de multimillonarios. Con la cultura de redes sociales,
smartphones, Windows y Machintosh es la situación actual.

La respuesta de clase es que obreros que puedan estar afiliados a un
sindicato pongan la industria del software, como propiedad colectiva y
autogestionaria, el software libre es propiedad colectiva. En un
proyecto de software libre independientemente de la ideología del
obrero, cualquiera puede copiar el software, modificarlo y compartirlo
con sus iguales. El problema con estas comunidades de software libre en
la actualidad es que no se separa a los trabajadores de los empresarios,
ni cargos de partido, ni fuerzas represivas, por lo que no llegan a ser
comunidades autogestionarias.

Las ventajas para los obreros del ramo del software de trabajar en
puestos desarrollando software libre son cosas como:

-   Reconocimiento de iguales cuando se comparte el software.
-   Muchos paquetes de software libre llegan a ser estándares, con lo
    cual facilita que el obrero se defienda ante cambios de empleo.

Las ventajas para las personas que estudian un paquete de software con
un libro libre con respecto libro con una licencia que hasta impide la
libre copia son:

-   Puesto que el libro no está sometido a una compra, me lo quedo para
    toda la vida, invierto en leerlo, ya sé que está ahí el
    conocimiento, nunca va a dejar de estar ahí en la versión que lo leí
-   Si encuentro fallos se pueden solucionar, los puedo resolver yo (no
    es muy difícil editar un texto en formatos de textos transparentes)
    u otra persona
-   Muchas veces es fácil encontrar una comunidad alrededor del libro,
    normalmente es la de la herramienta de software libre. Por eso en
    Libremanuals se apuesta por documentación libre oficial del paquete
    de software libre.

Con lo cual puede entenderse el software libre como una necesidad
obrera, sindicalista y de clase.

Libremanuals se centra en libros impresos de documentación libre en
castellano sobre software libre. La problemática con los manuales es la
misma que con el software, proyectos como Wikipedia demuestran la
necesidad y utilidad de la documentación libre para generar textos que
enseñen cosas con una licencia libre (propiedad colectiva y
autogestionaria). Las ventajas para la clase obrera y la persona que
documenta es la misma básicamente que con el software.

Vendiendo y traduciendo libros de software libre se puede generar un
proyecto autogestionario que defienda la cultura castellana en el mundo
del software con una perspectiva libertaria.

Author: David Arroyo Menéndez
Save_as: faq-freedoc.html
Title: Preguntas Frecuentes acerca de Documentación Libre
Url: faq-freedoc.html
Status: hidden

Preguntas Frecuentes acerca de Documentación Libre {#preguntas-frecuentes-acerca-de-documentación-libre .my-4}
==================================================

### [Índice]()

-   [¿Qué es documentación libre?](#que-es)
-   [¿Qué licencias protegen la documentación libre?](#licencias)
-   [Yo comparto mis documentos en Internet ¿por qué estos necesitan una
    licencia? ¿no son ahora más libres?](#yo-comparto)
-   [¿Cómo puedo ponerle una licencia libre a mis
    documentos?](#como-licencia)
-   [Tengo mi documento en un formato propietario como por ejemplo un
    .doc ¿puede mi documento ser libre?](#formato-propietario)
-   [No entiendo muy bien por qué puede haber secciones invariantes en
    textos libres](#secciones-invariantes)
-   [No entiendo muy bien eso del titular de la obra](#titular-obra)
-   [Yo soy profesor ¿cómo puedo apoyar la documentación
    libre?](#profesor-apoya)
-   [Como profesor en qué me beneficiaría generar documentación
    libre?](#profesor-beneficio)
-   [¿En qué beneficia la documentación libre a la educación y a la
    sociedad?](#educacion-beneficio)
-   [Yo soy estudiante ¿cómo puedo apoyar la documentación libre? ¿en
    qué me beneficiará?](#estudiante-apoya)
-   [Yo soy una editorial ¿bajo qué términos puedo editar documentación
    libre?](#editorial-terminos)
-   [¿Constituye la documentación libre un peligro para las editoriales
    tradicionales?](#peligro-editorial)
-   [¿Son gratis los libros libres?](#gratis-libre)
-   [¿Qué proyectos existen de documentación libre en castellano? ¿y en
    otros idiomas?](#proyectos)

### [¿Qué es documentación libre?]()

En breves palabras se puede decir que es la documentación que garantiza
la libre copia y modificación de sus contenidos, con la única
restricción de no modificar su licencia.

\[ [Volver al Índice](#indice) \]

### [¿Qué licencias protegen la documentación libre?]()

Las licencias más populares de documentación ó textos libres es la [GNU
Free Documentation License](http://www.gnu.org/licenses/fdl.html) y las
licencias [Creative Commons](http://www.creativecommons.org). Y aunque
también existen otras como:

-   [The FreeBSD Documentation
    License](http://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/ln16.html)

-   [The Apple's Common Documentation License, Version
    1.0](http://www.opensource.apple.com/cdl/)

-   [Open Publication License,
    Version 1.0.](http://opencontent.org/openpub/)

En [Creative Commons](http://creativecommons.org/) tienen un interfaz
muy sencillo, para que tú le apliques a tu obra de libre copia otras
libertades que tú consideras importantes: 1) dar crédito (ó no) al autor
original, 2) permitir (ó no) usos comerciales a terceros de tu obra y 3)
permitir (ó no) modificaciones a la obra original.

\[ [Volver al Índice](#indice) \]

### [Yo comparto mis documentos en Internet ¿por qué estos necesitan una licencia? ¿no son ahora más libres?]()

En realidad, no. Si tu pones un documento en Internet y no tiene ninguna
licencia, ni copyright, tus documentos tienen el mismo copyright
restrictivo que la mayoría de los libros impresos que puedes encontrar,
es decir, todos los derechos reservados para el titular de la obra (en
este caso el autor) de la obra y nadie no autorizado por éste puede
hacer copias (con las excepciones previstas por la ley, como el "fair
use", copia privada, etc). Por eso el anticopyright (rechazar poner
copyright a una obra) es poco útil para la comunidad: su efecto es nulo
y es una obra propietaria. Para "regalar" algo al público, hay que
hacerlo explicito, de acuerdo a la ley : eso es lo que nos facilitan
proyectos como [Creative Commons](http://creativecommons.org/).

\[ [Volver al Índice](#indice) \]

### [¿Cómo puedo ponerle una licencia libre a mis documentos?]()

Es muy sencillo. Primero debes de pensar si tu texto es un texto cerrado
que no puede ser mejorado o ampliado por nadie más que por ti mismo, ó
si se trata de un texto que puede tener una lectura abierta y, por ello,
mejorado por más personas. Si el texto, por ejemplo los apuntes de una
asignatura, pueden ser enriquecidos y mejorados y piensas que esas
aportaciones tienen la posibilidad de desarrollarse libre y
colectivamente, entonces la licencia GFDL es la adecuada.

Entonces lee con atención la licencia GFDL
<http://www.gnu.org/copyleft/fdl.html> y si no sabes mucho inglés, hay
traducciones no oficiales, pero debes tener en cuenta que en caso de que
una contradiga a la otra, la que vale es la versión en inglés.

Básicamente, el texto debe incluir las siguientes especificaciones:

*Para usar esta licencia en un documento que usted haya escrito, incluya
una copia de la Licencia en el documento y ponga el siguiente derecho de
autor y nota de licencia justo después de la página de título:*

*Derecho de Autor (c) AÑO SU NOMBRE. Se otorga permiso para copiar,
distribuir y/o modificar este documento bajo los términos de la Licencia
de Documentación Libre GNU, Versión 1.1 o cualquier otra versión
posterior publicada por la Free Software Foundation; con las Secciones
Invariantes siendo LISTE SUS TÍTULOS, con los Textos de Portada siendo
LISTELO, y con los Textos al respaldo de la página de título siendo
LISTELOS. Una copia de la licencia es incluida en la sección titulada
"Licencia de Documentación Libre GNU".*

*Si no tiene Secciones Invariantes, escriba "Sin Secciones Invariantes"
en vez de decir cuáles son invariantes. Si no tiene Texto de Portada,
escriba "Sin Texto de Portada" en vez de "con los Textos de Portada
siendo LISTELO"; e igualmente para los Textos al respaldo de la página
de título.*

Otras cuestiones importantes son:

-   Incluir en el documento impreso la referencia a las fuentes, algo
    parecido a: *Las fuentes de este documento están accesibles
    desde https://github.com/davidam/orgguide-es.*
-   Incluir la copia de la licencia

Si no te gusta la licencia GFDL, déjate guiar por el [interfaz de
Creative Commons para elegir
licencia](https://creativecommons.org/choose/).

\[ [Volver al Índice](#indice) \]

### [Tengo mi documento en un formato propietario como por ejemplo un .doc ¿puede mi documento ser libre?]()

Si bien [Libreoffice](http://www.libreoffice.org/) funciona hoy
razonablemente bien con los .doc, los formatos propietarios solo se
podrán visualizar y editar bien en un determinado procesador de textos.
Un documento libre no puede tener un formato privativo, puesto que ya no
sería libre. Antes de ponerle licencia GFDL, deberías pasarlos a un
formato no propietario.

Si tu documento no tiene gráficos, ni imágenes incrustadas guardando tu
documento como .rtf ó .txt será suficiente. En otro caso, te
recomendaría que utilizaras un formato más profesional, como puede ser
latex ó docbook. Estos formatos son fácilmente portables a formatos de
visualización/impresión como pdf, ps, dvi, etc. ó a otros formatos
también útiles como html, ó txt.

\[ [Volver al Índice](#indice) \]

### [No entiendo muy bien por qué puede haber secciones invariantes en textos libres]()

En ocasiones en un texto que se puede copiar y modificar libremente se
pueden incluir cosas, que no debería poder modificar cualquiera, como
pueden ser acuerdos colectivos (leyes, manifiestos, estatutos, etc.),
citas a obras clásicas ya sean éstas literarias ó científicas, etc.

Si por ejemplo tu haces un texto de físicas, dónde citas la teoría de la
relatividad de Einstein y, te has cerciorado de que Einstein la formuló
de ese modo, es posible, que no quieras que venga alguien y la modifique
por algo que ponga 2+2=6.

Las secciones invariantes permiten proteger a los documentos libres de
modificaciones de cosas que piensas que no tienen que modificarse. Ó
mejor dicho que sólo pueden modificarse por el titular de la obra, por
tanto deben usarse con mucho respeto tanto para los lectores, como para
los autores de la obra, y con el propio contexto del documento y las
posibilidades de que quieres dotarlo.

Las citas legales plantean en este sentido un problema ya que las leyes
suelen cambiar (últimamente, cada vez más, en sentido regresivo) y
declarar invariantes las partes de los documentos que reflejaran la
legislación vigente plantearía dificultades a la hora de actualizar el
documento. En ese sentido una solución sería enlazar desde el documento
a un sitio web donde se mantuvieran actualizados los textos legales, de
modo que el lector pudiera saber si esas citas legales están vigentes o
han sido modificadas. Otra posibilidad sería acotar, en ese punto muy
concreto, la licencia, estableciendo una clausula. En cualquier caso,
por la propia eficacia de los documentos en GFDL, parece más aconsejable
la primera solución, aunque en la actualidad en este estado aún no
contemos con esa mínima transparencia democrática que permitiera que
todos los textos legales estén disponibles gratuitamente en internet
para su lectura y discusión por parte de la ciudadanía.

\[ [Volver al Índice](#indice) \]

### [No entiendo muy bien eso del titular de la obra]()

El titular es aquel que disfruta de ciertos derechos de propiedad
intelectual de la obra. Por ejemplo: si tú disfrutas de unos textos
licenciados bajo GFDL el titular podría crear una versión modificada de
los mismos que tuvieran otra licencia. El titular puede ser una
organización ó una persona física.

Para más información acerca de la propiedad intelectual leer:

-   [La propiedad
    intelectual (mecd)](https://www.mecd.gob.es/cultura-mecd/areas-cultura/propiedadintelectual/la-propiedad-intelectual.html)
-   [¿Qué es la propiedad
    intelectual?](http://www.wipo.int/about-ip/es/)

Sin embargo, en un sentido ético, en un texto modificado y mejorado
comunitariamente, podríamos hablar de una titularidad colectiva, sin
perjucio de la titularidad individual y sin colisión con la propiedad
intelectual.

\[ [Volver al Índice](#indice) \]

### [Yo soy profesor ¿cómo puedo apoyar la documentación libre?]()

Un profesor juega un papel estratégico en la aceptación y difusión de la
documentación libre en nuestra sociedad. Es por ello que es tan
importante que si eres profesor, o conoces alguno, leas estas ideas para
apoyar la documentación libre:

-   Recomendar al alumnado la lectura y escritura ;) de documentación
    libre en las materias que impartes. Lo cual es algo que fomentará un
    aprendizaje activo y cooperativo entre el alumnado.
-   Realizar prácticas en las que el alumno pueda generar documentación
    libre con sus compañeros.
-   Liberando tus propios textos, artículos, tesis doctorales con algún
    tipo de licencia libre.

\[ [Volver al Índice](#indice) \]

### [Como profesor ¿en qué me beneficiaría generar documentación libre?]()

Cuando alguien genera un texto sobre un tema de investigación y lo
comparte a través de Internet, ese alguien se convierte en una autoridad
acerca del tema. Si lo hace utilizando una licencia libre recibirá una
realimentación mucho mayor, en forma de comentarios, mejoras directas en
el texto, ó incluso nuevos capítulos, ó secciones a añadir. De este
modo, su texto y usted se convertirán en una referencia para mucha
gente, durante bastante tiempo.

Como profesor quizás la primera pregunta no debiera ser en qué le
beneficia la documentación libre, sino también en qué beneficia a l@s
alumn@s, a la universidad, o a la educación, el que desde la docencia se
liberen textos y libros con licencias libres. En ese sentido, que en la
Universidad Pública, al menos, debiera ser el primordial, las licencias
libres presentan multitud de ventajas, tanto para el profesorado, como
para l@s estudiantes y la comunidad educativa: Se garantiza el acceso
universal a la enseñanza al posibilitar el uso de materiales de estudio
que de otra manera estarían vedados, por su alto coste, a importantes
capas de la población. Se realiza una de las grandes aspiraciones
pedagógicas que seguramente todo profesor guarda entre sus razones
vocacionales, la formación de individuos críticos, participativos,
libres. Se renueva ¡ se revoluciona! la educación tradicional entendida
como esa unipolaridad en la que el profesor emitía monotamente un
discurso y el alumno lo recepcionaba para a su vez volver a repetirlo:
con la participación en los materiales de estudio libres, el profesor
también aprende, y enriquece sus propios conocimientos y establece esa
dialéctica autocrítica que es el motor del progreso y del conocimiento.
Y por último, la licencia GFDL hace inutil la memorización, la
repetición, un texto libre sólo puede aprenderse, conocerse, leerse.

\[ [Volver al Índice](#indice) \]

### [¿En qué beneficia la documentación libre a la educación y a la sociedad?]()

En primer lugar, los beneficios de la documentación libre no son solo la
posibilidad de adquirir gratuitamente materiales de estudio por parte
del alumnado. Las licencias libres son mucho más que eso y resultan
útiles porque resultan fundamentales para entender dos principios
básicos, no sólo de la educación, sino en general de cualquier tipo de
lectura y escritura, que son: el conocimiento y el aprendizaje.

Las licencias libres hacen posible el aprendizaje compartido, el sentido
crítico, la lectura colectiva, el respeto al otro y a los otros a través
de la escritura en común. Conceptos que parecían vacíos porque el
sistema educativo no había, hasta ahora, encontrado las herramientas que
los hicieran posibles. La distancia autor-lector, la edificación, por
parte del imaginario colectivo, de personalidades creativas de caracter
mítico, "el genio", emisoras de un conocimiento extraordinario e
irrepetible que apenas necesitarán de un pobre receptor al otro lado de
la escritura, todo ello queda abolido por el principio de libertad del
copyleft.

De este modo, la formación del saber en nuestra sociedad no tiene por
qué ser solo obra de "visionarios" sino de la colectividad como sujeto e
instrumento de su propio saber y de su propio progreso. En esto el
ámbito educativo es básico, puesto que puede optar por formar a los
individuos en la tradición cultural, y por tanto perpetuar un sistema
propietario de la cultura, o por formar a las persona libremente,
facilitando las herramientas y los medios para que las personas tengan
al menos la posibilidad de ser personas libres.

\[ [Volver al Índice](#indice) \]

### [Yo soy estudiante ¿cómo puedo apoyar la documentación libre? ¿en qué me beneficiará?]()

El modo que un estudiante tiene de apoyar la documentación libre son
varias:

-   Participar en la elaboración de materiales libres como elementos
    complementarios a su aprendizaje.
-   Informar al profesorado de materiales libres que puede utilizar en
    sus asignaturas.
-   Otorgar una licencia libre y publicar en internet los trabajos que
    realice en sus estudios (prácticas, proyectos de fin de carrera,
    tesis doctorales, etc.).

Los beneficios de estas acciones serían los siguientes:

-   Al participar en la elaboración de materiales libres de manera
    complementaria a su aprendizaje el alumno expresa ante una comunidad
    sus propias ideas y cultura de cómo ha entendido un concepto y ésta
    comunidad da su aprobación, matiza, ó mejora lo expresado.
-   Al informar al profesorado de materiales libres, el/la estudiante
    encontrará orientación de cómo utilizarlos en el contexto de la
    asignatura, así mismo el/la docente recibirá de manera positiva el
    interés mostrado
-   Otorgando una licencia libre y dándole difusión apropiada a un
    trabajo es muy probable que el/la estudiante se vuelve en un
    referente de lo aprendido para muchas personas, también recibirá
    realimentación al mismo en forma de comentarios y mejoras al mismo.

\[ [Volver al Índice](#indice) \]

### [Yo soy una editorial ¿bajo qué términos puedo editar documentación libre?]()

Le recomendaría que leyera la [GNU Free Documentation
License](http://www.gnu.org/licenses/fdl.html), poniendo especial
interés en los puntos 2 y 3 "VERBATIM COPYING" (*COPIA LITERAL*) y
"COPYING IN QUANTITY" (*COPIADO EN CANTIDAD*).

\[ [Volver al Índice](#indice) \]

### [¿Constituye la documentación libre un peligro para las editoriales tradicionales?]()

Hay varias fórmulas desde las que las empresas editoriales hacen negocio
con la documentación libre.

Es posible que navegando por internet (ó de cualquier otro modo) una
editorial se encuentre con un texto bajo una licencia copyleft que por
sus contenidos y su aceptación piense que es posible venderlo. Hoy día
hay editoriales como la americana O'Reilly ó la española Traficantes de
Sueños que tienen un buen número de libros editados de este modo.

Otra fórmula que utiliza la editorial italiana Mondadori es utilizar una
licencia que permita la copia y modificación electrónica de los
documentos, pero bajo la que se reserve la exclusiva de la edición
impresa.

Puesto que es posible hacer negocio de la documentación libre, las
editoriales deberían ver la documentación libre como una oportunidad que
eso sí, tal vez les haga replantearse ciertos aspectos de su modelo de
negocio.

\[ [Volver al Índice](#indice) \]

### [¿Son gratis los libros libres?]()

Pueden ser adquiridos gratuitamente ó no.

Los libros, textos, artículos, tesis, liberados con licencias libres
pueden ser bajados sin coste de la red (si tienes acceso a tarifas ó
semiplanas), pueden ser copiados y modificados respetando las licencias
libres, pero también pueden ser adquiridos pagando un precio.

En general a la gente le resulta más comodo para el estudio que adquirir
el libro editado normalmente y a muchos no les importará pagar por ello.
También habrá otros que querrán evitarse el trabajo de recopilar y
descargarse documentos libres de internet y pagarán por tener un cd con
todo el contenido que quieren, de la misma manera que ocurre con las
distribuciones de software libre. Seguramente surjan otros modelos de
negocio en relación a la documentación libre que ahora no podemos
preveer.

Los libros continuarán existiendo también en su configuración actúal,
pero las licencias serán libres, lo cual redundará en su coste, pero
sobre todo en su calidad, ya que los contenidos estarán permanentemente
actualizados gracias a la lectura crítica y participativa.

\[ [Volver al Índice](#indice) \]
